# -*- coding: utf-8 -*-
""" ------------------------------------------------------------------------------------
	Cortical Basal Ganglia Neurons: file containing classes for defining network neurons
	------------------------------------------------------------------------------------
	
								Model References 
	------------------------------------------------------------------------------------
	Cortical Pyramical Cell Soma:
	Pospischil, M., Toledo-Rodriguez, M., Monier, C., Piwkowska, Z., 
	Bal, T., Frégnac, Y., Markram, H. and Destexhe, A., 2008. 
	"Minimal Hodgkin–Huxley type models for different classes of 
	cortical and thalamic neurons." 
	Biological cybernetics, 99(4-5), pp.427-441.
	
	Cortical Pyramidal Cell Axon: 
	Foust, A.J., Yu, Y., Popovic, M., Zecevic, D. and McCormick, D.A., 
	2011. "Somatic membrane potential and Kv1 channels control spike 
	repolarization in cortical axon collaterals and presynaptic boutons." 
	Journal of Neuroscience, 31(43), pp.15490-15498.
	
	Cortical Interneurons:
	Pospischil, M., Toledo-Rodriguez, M., Monier, C., Piwkowska, Z., 
	Bal, T., Frégnac, Y., Markram, H. and Destexhe, A., 2008. 
	"Minimal Hodgkin–Huxley type models for different classes of 
	cortical and thalamic neurons." 
	Biological cybernetics, 99(4-5), pp.427-441.
	
	Implemented by John Fleming - john.fleming@ucdconnect.ie - 06/12/18
	
	Edits: 16-01-19: Created classes for cell models so they can be 
					 utilized in PyNN.

	Created on Tues Jan 15 12:51:26 2019

"""

from math import pi
from neuron import h
from nrnutils import Mechanism, Section
from pyNN.neuron import NativeCellType
from pyNN.parameters import Sequence
import numpy as np
from scipy import signal

try:
	reduce
except NameError:
	from functools import reduce

def _new_property(obj_hierarchy, attr_name):
	"""
	Returns a new property, mapping attr_name to obj_hierarchy.attr_name.

	For example, suppose that an object of class A has an attribute b which
	itself has an attribute c which itself has an attribute d. Then placing
		e = _new_property('b.c', 'd')
	in the class definition of A makes A.e an alias for A.b.c.d
	"""

	def set(self, value):
		obj = reduce(getattr, [self] + obj_hierarchy.split('.'))
		setattr(obj, attr_name, value)

	def get(self):
		obj = reduce(getattr, [self] + obj_hierarchy.split('.'))
		return getattr(obj, attr_name)
	return property(fset=set, fget=get)

def generate_DBS_Signal(start_time, stop_time, dt, amplitude, frequency, pulse_width, offset):
	# Not efficient at all... Is there a way to have those vectors computed on the fly ?
	# Otherwise should have a buffer mechanism
	times = np.round(np.arange(start_time, stop_time, dt), 2)
	tmp = np.arange(0, stop_time - start_time, dt)/1000.0
		
	# Calculate the duty cycle of the DBS signal
	T = (1.0/frequency)*1000.0	# time is in ms, so *1000 is conversion to ms
	duty_cycle = ((pulse_width)/T)
	#DBS_Signal = offset + amplitude * (1.0+signal.square(2.0 * np.pi * frequency * tmp, duty=duty_cycle))/2.0
	DBS_Signal = offset + 1.0 * (1.0+signal.square(2.0 * np.pi * frequency * tmp, duty=duty_cycle))/2.0 		# Need to initially set value > 0 to find last spike time, then can scale by amplitude
	DBS_Signal[-1] = 0.0
	
	# Calculate the time for the first pulse of the next segment
	last_pulse_index = np.where(np.diff(DBS_Signal)>0)[0][-1]
	next_pulse_time = times[last_pulse_index] + T
	
	# Rescale amplitude
	DBS_Signal *= amplitude
	
	return DBS_Signal, times, next_pulse_time

class Cortical_Neuron(object):
	
	def __init__(self, **parameters):
				
		self.soma = Section(L=parameters['soma_L'], diam=parameters['soma_diam'], nseg=parameters['soma_nseg'], Ra=parameters['soma_Ra'], cm=parameters['soma_cm'],
							mechanisms=(Mechanism('cortical_soma_i_leak'), Mechanism('cortical_soma_i_na'), Mechanism('cortical_soma_i_k'), Mechanism('cortical_soma_i_m')))
		
		# Multiplication by 1e-4 is to convert units from Eleanor's model (pS/um2 -> S/cm2)
		self.ais = Section(L=parameters['ais_L'], diam=parameters['ais_diam'], nseg=parameters['ais_nseg'], Ra=parameters['ais_Ra'], cm=parameters['ais_cm'],
							mechanisms=(Mechanism('cortical_axon_i_leak', g_l=3.3e-5), Mechanism('cortical_axon_i_na', g_Na=4000e-4), 
										#Mechanism('cortical_axon_i_kv', g_Kv=20e-4), Mechanism('cortical_axon_i_kd', g_Kd=0.015)))
										Mechanism('cortical_axon_i_kv', g_Kv=20e-4), Mechanism('cortical_axon_i_kd', g_Kd=0.015)), parent=self.soma)

		
		# Use loop to create myelin and node sections of axon
		self.myelin = []
		self.node = []
		for i in np.arange(parameters['num_axon_compartments']):
			if i==0: 
				self.myelin.append(Section(L=parameters['myelin_L'], diam=parameters['myelin_diam'], nseg=11, Ra=parameters['myelin_Ra'], cm=parameters['myelin_cm'],
							#mechanisms=(Mechanism('cortical_axon_i_leak', g_l=3.3e-5), Mechanism('cortical_axon_i_na', g_Na=0.001)),
							mechanisms=(Mechanism('cortical_axon_i_leak', g_l=0), Mechanism('cortical_axon_i_na', g_Na=10e-4)),
							parent=self.ais))
			else:
				self.myelin.append(Section(L=parameters['myelin_L'], diam=parameters['myelin_diam'], nseg=11, Ra=parameters['myelin_Ra'], cm=parameters['myelin_cm'],
							mechanisms=(Mechanism('cortical_axon_i_leak', g_l=0), Mechanism('cortical_axon_i_na', g_Na=10e-4)),
							parent=self.node[i-1]))
			
			self.node.append(Section(L=parameters['node_L'], diam=parameters['node_diam'], nseg=parameters['node_nseg'], Ra=parameters['node_Ra'], cm=parameters['node_cm'],
							mechanisms=(Mechanism('cortical_axon_i_leak', g_l=0.02), Mechanism('cortical_axon_i_na', g_Na=2800e-4),
										Mechanism('cortical_axon_i_kv', g_Kv=5e-4), Mechanism('cortical_axon_i_kd', g_Kd=0.0072)),
										parent=self.myelin[i]))
										
		self.collateral = Section(L=parameters['collateral_L'], diam=parameters['collateral_diam'], nseg=parameters['collateral_nseg'], Ra=parameters['collateral_Ra'], cm=parameters['collateral_cm'],
							mechanisms=(Mechanism('cortical_axon_i_leak'), Mechanism('cortical_axon_i_na', g_Na=1333.33333e-4), 
										#Mechanism('cortical_axon_i_kv', g_Kv=10e-4), Mechanism('cortical_axon_i_kd', g_Kd=0.006)))
										Mechanism('cortical_axon_i_kv', g_Kv=10e-4), Mechanism('cortical_axon_i_kd', g_Kd=0.006)), parent=self.node[-1])
		
		middle_index = int((parameters['num_axon_compartments']/2.0))
		self.middle_node = self.node[middle_index]
		self.middle_myelin = self.myelin[middle_index]
		
		# Add extracellular (and xtra) mechanism to collateral 
		self.collateral.insert('extracellular')
		self.collateral.insert('xtra')
		
		# Assign rx values to the segments rx_xtra
		for seg in self.collateral :
			seg.xtra.rx = seg.x*3e-1
		
		# Maybe above was wrong when setting pointers
		for seg in self.collateral:
			h.setpointer(seg._ref_e_extracellular, 'ex', seg.xtra)
			h.setpointer(seg._ref_i_membrane, 'im', seg.xtra)
		
		# Add bias current to neuron model - current amplitude is in terms of original model paper, nA 
		# 									 i.e. no need to convert from current density like for other CBG cell models
		# insert current source
		self.stim = h.IClamp(0.5, sec=self.soma)
		self.stim.delay = 0
		self.stim.dur = 1e12
		self.stim.amp = parameters['soma_bias_current_amp']
		
		# insert synaptic noise 
		self.noise = h.SynNoise(0.5, sec=self.soma)
		self.noise.f0 = 0
		self.noise.f1 = 0.3
		
		#print(self.myelin)
		
		# Add AMPA and GABAa synapses to the cell, i.e. add to the soma section
		self.AMPA = h.AMPA_S(0.5, sec=self.soma)
		self.GABAa = h.GABAa_S(0.5, sec=self.soma)
		
		# needed for PyNN
		self.source = {'soma': self.soma(0.5)._ref_v, 'middle_axon_node': self.middle_node(0.5)._ref_v, 'collateral': self.collateral(0.5)._ref_v}
		self.source_section = {'soma': self.soma, 'middle_axon_node': self.middle_node, 'collateral': self.collateral}
		self.rec = h.NetCon(self.source['collateral'], None, sec=self.source_section['collateral'])		# Needed to clear the simulator
		self.spike_times = h.Vector(0)
		self.traces = {}
		self.recording_time = False
		self.parameter_names = ()
		"""
		self.parameter_names = ('soma_L', 'soma_diam', 'soma_nseg', 'soma_Ra', 'soma_cm', 'soma_bias_current_amp',
								'ais_L', 'ais_diam', 'ais_nseg', 'ais_Ra', 'soma_cm',
								'myelin_L_0', 'myelin_L', 'myelin_diam', 'myelin_nseg_0', 'myelin_nseg', 'myelin_Ra', 'myelin_cm',
								'collateral_L', 'collateral_diam', 'collateral_nseg', 'collateral_Ra', 'collateral_cm')
		"""
	
	"""
	# Soma Compartment Properties
	soma_bias_current_amp = _new_property('stim', 'amp')
	soma_L = _new_property('soma', 'L')
	soma_diam = _new_property('soma', 'diam')
	soma_nseg = _new_property('soma', 'nseg')
	soma_Ra = _new_property('soma', 'Ra')
	soma_cm = _new_property('soma', 'cm')
	
	# AIS Compartment Properties
	ais_L = _new_property('ais', 'L')
	ais_diam = _new_property('ais', 'diam')
	ais_nseg = _new_property('ais', 'nseg')
	ais_Ra = _new_property('ais', 'Ra')
	ais_cm = _new_property('ais', 'cm')
	
	# Myelin Compartment Properties
	myelin_L_0 = _new_property('myelin[0]', 'L')
	myelin_L = _new_property('myelin[1]', 'L')
	myelin_diam = _new_property('myelin[1]', 'diam')
	myelin_nseg_0 = _new_property('myelin[0]', 'nseg')
	myelin_nseg = _new_property('myelin[1]', 'nseg')
	myelin_Ra = _new_property('myelin[1]', 'Ra')
	myelin_cm = _new_property('myelin[1]', 'cm')
		
	# Collateral Compartment Properties
	collateral_L = _new_property('collateral', 'L')
	collateral_diam = _new_property('collateral', 'diam')
	collateral_nseg = _new_property('collateral', 'nseg')
	collateral_Ra = _new_property('collateral', 'Ra')
	collateral_cm = _new_property('collateral', 'cm')
	"""
	
	def soma_area(self):
		"""Membrane area in µm²"""
		return pi * self.soma.L * self.soma.diam
	
	def memb_init(self):
		for seg in self.soma:
			seg.v = self.v_init
	
	def _set_collateral_rx(self, sequence_values):
		rx_values = sequence_values.value
		for ii, seg in enumerate(self.collateral):
			seg.xtra.rx = rx_values[ii]
		
	def _get_collateral_rx(self):
		print("Getter Working!")
		rx_values = np.zeros((1,self.collateral.nseg))
		for i, seg in enumerate(self.collateral):
			rx_values[0,i] = seg.xtra.rx
		print(Sequence(rx_values.flatten()))
		
	collateral_rx = property(fget=_get_collateral_rx, fset=_set_collateral_rx)
	
	
class Cortical_Neuron_Type(NativeCellType):
	#default_parameters = {'soma_L': 35, 'soma_diam': 25, 'soma_nseg': 1, 'soma_Ra': 100, 'soma_cm': 1, 'soma_bias_current_amp': 0.12,
	default_parameters = {'soma_L': 35, 'soma_diam': 25, 'soma_nseg': 1, 'soma_Ra': 150, 'soma_cm': 1, 'soma_bias_current_amp': 0.12,
							'ais_L': 20, 'ais_diam': 1.2, 'ais_nseg': 5, 'ais_Ra': 150, 'ais_cm': 0.8,
							'myelin_L': 500, 'myelin_L_0': 80, 'myelin_diam': 1.4, 'myelin_Ra': 150, 'myelin_cm': 0.04,
							'node_L': 2, 'node_diam': 1.2, 'node_nseg': 1,'node_Ra': 150, 'node_cm': 0.8,
							'collateral_L': 500, 'collateral_diam': 0.5, 'collateral_nseg': 11, 'collateral_Ra': 150, 'collateral_cm': 0.8,
							'num_axon_compartments': 10}
	
	# Define initial vector of transfer resistances for the collateral segments
	initial_collateral_rx = np.zeros((1,default_parameters['collateral_nseg'])).flatten()
	initial_collateral_rx_Sequence = Sequence(initial_collateral_rx)
	default_parameters['collateral_rx'] = initial_collateral_rx_Sequence
	
	default_initial_values = {'v': -68.0}
	recordable = ['soma(0.5).v','collateral(0.5).v', 'collateral(0.5).i_membrane_', 'ais(0.5).v', 'middle_node(0.5).v' , 'middle_myelin(0.5).v', 'AMPA.i', 'GABAa.i']
	units = {'soma(0.5).v' : 'mV', 'collateral(0.5).v': 'mV', 'collateral(0.5).i_membrane_': 'nA', 'ais(0.5).v': 'mV', 'middle_node(0.5).v': 'mV' , 'middle_myelin(0.5).v': 'mV', 'AMPA.i': 'nA', 'GABAa.i': 'nA'}    
	receptor_types = ['AMPA', 'GABAa']
	model = Cortical_Neuron

class Interneuron(object):
	
	def __init__(self, **parameters):
		
		# Create single compartment Destexhe Interneuron cell section, i.e. soma section
		self.soma = Section(L=parameters['L'], diam=parameters['diam'], nseg=parameters['nseg'], Ra=parameters['Ra'], cm=parameters['cm'],
							mechanisms=(Mechanism('interneuron_i_leak'), Mechanism('interneuron_i_na'), Mechanism('interneuron_i_k')))
		
		# Add bias current to neuron model - current amplitude is in terms of original model paper, nA 
		# 									 i.e. no need to convert from current density like for other CBG cell models
		# insert current source
		self.stim = h.IClamp(0.5, sec=self.soma)
		self.stim.delay = 0
		self.stim.dur = 1e12
		self.stim.amp = parameters['bias_current_amp']	# nA
		
		# insert synaptic noise 
		self.noise = h.SynNoise(0.5, sec=self.soma)
		self.noise.f0 = 0
		self.noise.f1 = 0.3
		
		# Add AMPA and GABAa synapses to the cell, i.e. add to the soma section
		self.AMPA = h.AMPA_S(0.5, sec=self.soma)
		self.GABAa = h.GABAa_S(0.5, sec=self.soma)
		
		# needed for PyNN
		self.source_section = self.soma
		self.source = self.soma(0.5)._ref_v
		self.rec = h.NetCon(self.source, None, sec=self.source_section)		# Needed to clear the simulator
		self.spike_times = h.Vector(0)
		self.parameter_names = ('L', 'diam', 'nseg', 'Ra', 'cm', 'bias_current_amp')
		self.traces = {}
		self.recording_time = False

	L = _new_property('soma', 'L')
	diam = _new_property('soma', 'diam')
	nseg = _new_property('soma', 'nseg')
	Ra = _new_property('soma', 'Ra')
	cm = _new_property('soma', 'cm')
	bias_current_amp = _new_property('stim', 'amp')
	
	def area(self):
		"""Membrane area in µm²"""
		return pi * self.soma.L * self.soma.diam
	
	def memb_init(self):
		for seg in self.soma:
			seg.v = self.v_init

class Interneuron_Type(NativeCellType):
	default_parameters = {'L': 35, 'diam': 25, 'nseg': 1, 'Ra': 150, 'cm': 1, 'bias_current_amp': 0.25}
	default_initial_values = {'v': -68.0}
	recordable = ['soma(0.5).v']
	units = {'soma(0.5).v' : 'mV'}    
	receptor_types = ['AMPA', 'GABAa']
	model = Interneuron

class STN_Neuron(object):
	
	def __init__(self, **parameters):
		
		# Create single compartment Otsuka STN cell section, i.e. soma section
		self.soma = Section(L=parameters['L'], diam=parameters['diam'], nseg=parameters['nseg'], Ra=parameters['Ra'], cm=parameters['cm'],
							mechanisms=[Mechanism('myions'), Mechanism('stn', gnabar=49e-3, gkdrbar=57e-3, gkabar=5e-3, gkcabar=1.0e-3, gcalbar=15e-3,
														gcatbar=5e-3, kca=2, gl=0.35e-3)])
		# Initialize ion concentrations
		h("cai0_ca_ion = 5e-6 ")
		h("cao0_ca_ion = 2")
		h("ki0_k_ion = 105") 
		h("ko0_k_ion = 3")
		h("nao0_na_ion = 108")
		h("nai0_na_ion = 10")
		
		# Add bias current to neuron model
		self.stim = h.IClamp(0.5, sec=self.soma)
		self.stim.delay = 0
		self.stim.dur = 1e12
		self.stim.amp = parameters['bias_current']			# bias current density (nA)
		
		"""
		# Add AMPA and GABAa synapses to the cell, i.e. add to the soma section
		self.AMPA_Synapse_List = [h.AMPA_S(0.5, sec=self.soma) for i in range(parameters['num_AMPA_Synapses'])]
		
		print("List:")
		print(self.AMPA_Synapse_List)
		print("Single:")
		print(self.AMPA_Synapse_List[0])
		
		self.AMPA = h.AMPA_S(0.5, sec=self.soma)
		self.GABAa = h.GABAa_S(0.5, sec=self.soma)
		print(self.AMPA)
		"""
		self.AMPA = h.AMPA_S(0.5, sec=self.soma)
		self.GABAa = h.GABAa_S(0.5, sec=self.soma)
		
		
		# needed for PyNN
		self.source_section = self.soma
		self.source = self.soma(0.5)._ref_v
		self.rec = h.NetCon(self.source, None, sec=self.soma)		# Needed to clear the simulator
		self.spike_times = h.Vector(0)
		self.parameter_names = ('L', 'diam', 'nseg', 'Ra', 'cm', 'bias_current')
		self.traces = {}
		self.recording_time = False

	L = _new_property('soma', 'L')
	diam = _new_property('soma', 'diam')
	nseg = _new_property('soma', 'nseg')
	Ra = _new_property('soma', 'Ra')
	cm = _new_property('soma', 'cm')
	bias_current_amp = _new_property('stim', 'amp')
	
	def area(self):
		"""Membrane area in µm²"""
		return pi * self.soma.L * self.soma.diam
	
	def memb_init(self):
		for seg in self.soma:
			seg.v = self.v_init
	

class STN_Neuron_Type(NativeCellType):
	default_parameters = {'L': 60, 'diam': 60, 'nseg': 1, 'Ra': 200, 'cm': 1, 'bias_current': 0.0, 'num_AMPA_Synapses': 5, 'num_GABAa_Synapses': 5}
	default_initial_values = {'v': -68.0}
	#AMPA_Synapse_List[0].i
	#AMPA_Synapse_List[1].i
	recordable = ['soma(0.5).v', 'AMPA.i', 'GABAa.i']
	units = {'soma(0.5).v' : 'mV', 'AMPA.i': 'nA', 'GABAa.i': 'nA'}    
	receptor_types = ['AMPA', 'GABAa']
	model = STN_Neuron

class GP_Neuron(object):
	
	"""
	DBS_amplitudes = []
	DBS_times = []
	DBS_next_pulse_time = []
	DBS_amplitudes_neuron = []
	DBS_times_neuron = []
	updated_DBS_signal = []
	"""
	
	def __init__(self, **parameters):
		
		# Create single compartment Rubin and Terman GP cell section, i.e. soma section
		self.soma = Section(L=parameters['L'], diam=parameters['diam'], nseg=parameters['nseg'], Ra=parameters['Ra'], cm=parameters['cm'],
							mechanisms=[Mechanism('myions'), Mechanism('GPeA', gnabar=0.04, gkdrbar=0.0042, gkcabar=0.1e-3,
														gcatbar=6.7e-5, kca=2, gl=4e-5)])
		
		# Initialize ion concentrations
		h("cai0_ca_ion = 5e-6 ")
		h("cao0_ca_ion = 2")
		h("ki0_k_ion = 105") 
		h("ko0_k_ion = 3")
		h("nao0_na_ion = 108")
		h("nai0_na_ion = 10")
		
		# insert current source
		self.stim = h.IClamp(0.5, sec=self.soma)
		self.stim.delay = 0
		self.stim.dur = 1e12
		self.stim.amp = parameters['bias_current']
		
		# Add DBS stimulation current to neuron model
		self.DBS_stim = h.IClamp(0.5, sec=self.soma)
		self.DBS_stim.delay = 0
		self.DBS_stim.dur = 1e9
		self.DBS_stim.amp = 0
		
		"""
		self.DBS_amplitudes = []
		self.DBS_times = []
		self.DBS_next_pulse_time = []
		self.DBS_amplitudes_neuron = []
		self.DBS_times_neuron = []
		"""
		
		"""
		# Create DBS signal to be played to GPe neuron model
		self.DBS_amplitudes, self.DBS_times, self.DBS_next_pulse_time = generate_DBS_Signal(0, 30000, 0.01, 100.0, 130.0, 0.06, 0)
		
		# Initially set the stimulation amplitude to 0
		self.DBS_amplitudes = 0*self.DBS_amplitudes
		
		# Make neuron vectors to be played to the 
		self.DBS_amplitudes_neuron = h.Vector(self.DBS_amplitudes)
		self.DBS_times_neuron = h.Vector(self.DBS_times)
	
		# Play DBS signal to global variable is_xtra
		self.DBS_amplitudes_neuron.play(self.DBS_stim._ref_amp, self.DBS_times_neuron, 1)
		#self.updated_DBS_signal = self.DBS_amplitudes_neuron.as_numpy()
		self.updated_DBS_signal = self.DBS_amplitudes_neuron.as_numpy()
		"""
		# Append the DBS stimulation iclamps to global list
		#GV.GPe_DBS_Signals.append(self.updated_DBS_signal)
		GV.GPe_stimulation_iclamps.append(self.DBS_stim)
		
		"""
		if GV.GPe_DBS_Signals.shape[0] == 0:
			GV.GPe_DBS_Signals = self.DBS_amplitudes_neuron.as_numpy()
		else:
			GV.GPe_DBS_Signals = np.vstack((GV.GPe_DBS_Signals, self.DBS_amplitudes_neuron.as_numpy()))
		"""
		
		""" # Play DBS signal into GPe neuron
		# Create DBS signal to be played to GPe neuron model
		self.DBS_amplitudes, self.DBS_times, self.DBS_next_pulse_time = generate_DBS_Signal(0, 30000, 0.01, 100.0, 130.0, 0.06, 0)
		
		# Initially set the stimulation amplitude to 0
		self.DBS_amplitudes = 0*self.DBS_amplitudes
		
		# Make neuron vectors to be played to the 
		self.DBS_amplitudes_neuron = h.Vector(self.DBS_amplitudes)
		self.DBS_times_neuron = h.Vector(self.DBS_times)
	
		# Play DBS signal to global variable is_xtra
		self.DBS_amplitudes_neuron.play(self.DBS_stim._ref_amp, self.DBS_times_neuron, 1)
		#print(self.DBS_amplitudes_neuron)
		
		# Get DBS_Signal_neuron as a numpy array for easy updating
		self.updated_DBS_signal = self.DBS_amplitudes_neuron.as_numpy()
		#print(len(self.updated_DBS_signal))
		"""
		
		# Add AMPA and GABAa synapses to the cell, i.e. add to the soma section
		self.AMPA = h.AMPA_S(0.5, sec=self.soma)
		self.GABAa = h.GABAa_S(0.5, sec=self.soma)
		
		# needed for PyNN
		self.source_section = self.soma
		self.source = self.soma(0.5)._ref_v
		self.rec = h.NetCon(self.source, None, sec=self.soma)		# Needed to clear the simulator
		self.spike_times = h.Vector(0)
		self.parameter_names = ('L', 'diam', 'nseg', 'Ra', 'cm', 'bias_current_density')
		self.traces = {}
		self.recording_time = False
	
	L = _new_property('soma', 'L')
	diam = _new_property('soma', 'diam')
	nseg = _new_property('soma', 'nseg')
	Ra = _new_property('soma', 'Ra')
	cm = _new_property('soma', 'cm')
	bias_current = _new_property('stim', 'amp')
	#DBS_Stim_current = _new_property('updated_DBS_signal')
	
	"""
	def stimulate(self, stim_signal, stim_times):
		""Method to stimulate neuron with stim_signal, between stim_times[0] (the start time index) and stim_times[1] (the end time index)""
		self.updated_DBS_signal[stim_times[0]:stim_times[1]] = stim_signal
	"""
	
	def _set_stimulation_signal(self, stimulation_sequence_parameter_values):
		
		"""
		stimulation_parameter_values = stimulation_sequence_parameter_values.value
		self.DBS_times = stimulation_parameter_values[0].value
		self.DBS_amplitudes = stimulation_parameter_values[1].value
		print(self.DBS_times)
		print(self.DBS_amplitudes)
		# Create neuron vector to play into 
		self.DBS_amplitudes_neuron = h.Vector(self.DBS_amplitudes)
		self.DBS_times_neuron = h.Vector(self.DBS_times)
	
		# Play DBS signal to global variable is_xtra
		self.DBS_amplitudes_neuron.play(self.DBS_stim._ref_amp, self.DBS_times_neuron, 1)
		"""
		self.updated_DBS_signal = self.DBS_amplitudes_neuron.as_numpy()
		
		#In stimulation - trying to change only part of the preconstructed DBS signal
		stimulation_parameter_values = stimulation_sequence_parameter_values.value
		stimulation_window_start_id = int(stimulation_parameter_values[0])
		stimulation_window_end_id = int(stimulation_parameter_values[1])
		stimulation_signal = stimulation_parameter_values[2::]
		#print(DBS_amplitudes_neuron)
		#updated_DBS_signal = self.DBS_amplitudes_neuron.as_numpy()
		#print(updated_DBS_signal)
		#self.updated_DBS_signal = self.DBS_amplitudes_neuron.as_numpy()
		#self.updated_DBS_signal[stimulation_window_start_id:stimulation_window_end_id] = np.array(stimulation_signal)
		#updated_DBS_signal = self.DBS_amplitudes_neuron.as_numpy()
		#print(self.DBS_amplitudes_neuron)
		#print(self.updated_DBS_signal[stimulation_window_start_id:stimulation_window_end_id])
		#self.updated_DBS_signal[stimulation_window_start_id:stimulation_window_end_id] = np.array(stimulation_signal)
		self.updated_DBS_signal[stimulation_window_start_id:stimulation_window_end_id] = 0
		#print(self.updated_DBS_signal[stimulation_window_start_id:stimulation_window_end_id])
		
		
	def _get_stimulation_signal(self):
		print(Sequence(self.DBS_amplitudes))
		
	stimulation = property(fget=_get_stimulation_signal, fset=_set_stimulation_signal)
	
	def area(self):
		"""Membrane area in µm²"""
		return pi * self.soma.L * self.soma.diam
	
	def memb_init(self):
		for seg in self.soma:
			seg.v = self.v_init
	

class GP_Neuron_Type(NativeCellType):
	default_parameters = {'L': 60, 'diam': 60, 'nseg': 1, 'Ra': 200, 'cm': 1.0, 'bias_current': 0.03}
	
	# Define initial vector for DBS stimulation being played to the GPe neurons
	initial_DBS_amplitudes, initial_DBS_times, initial_DBS_next_pulse_time = generate_DBS_Signal(0, 30000, 0.01, 0.0, 130.0, 0.06, 0)
	initial_DBS_amplitudes = list(initial_DBS_amplitudes)
	initial_DBS_amplitudes.insert(0, len(initial_DBS_amplitudes)-1)
	initial_DBS_amplitudes.insert(0, 0)
	
	initial_DBS_stimulation_Sequence = Sequence(initial_DBS_amplitudes)
	#print(len(initial_DBS_stimulation_Sequence))
	#print(len(initial_DBS_stimulation_Sequence.value))
	
	default_parameters['stimulation'] = initial_DBS_stimulation_Sequence
	
	default_initial_values = {'v': -68.0}
	recordable = ['soma(0.5).v']
	units = {'soma(0.5).v' : 'mV'}    
	receptor_types = ['AMPA', 'GABAa']
	model = GP_Neuron

class Thalamic_Neuron(object):
	
	def __init__(self, **parameters):
		
		# Create single compartment Rubin and Terman Thalamic cell section, i.e. soma section
		self.soma = Section(L=parameters['L'], diam=parameters['diam'], nseg=parameters['nseg'], Ra=parameters['Ra'], cm=parameters['cm'],
							mechanisms=(Mechanism('thalamic_i_leak'), Mechanism('thalamic_i_na_k'), Mechanism('thalamic_i_t')))
		
		"""
		# Optional add bias current to neuron model - current density is in terms of original model paper, pA/um2 
		# Note: Thalamic current has no bias current in paper, i.e. bias_current_density = 0
		# insert current source
		self.stim = h.IClamp(0.5, sec=self.soma)
		self.stim.delay = 0
		self.stim.dur = 1e12
		self.stim.amp = parameters['bias_current_density']*(self.area())*(0.001) 	# (0.001 or 1e-3) is conversion factor so pA -> nA
		"""
		
		# insert synaptic noise 
		self.noise = h.SynNoise(0.5, sec=self.soma)
		self.noise.f0 = 0
		self.noise.f1 = 0.3
		
		# Add AMPA and GABAa synapses to the cell, i.e. add to the soma section
		self.AMPA = h.AMPA_S(0.5, sec=self.soma)
		self.GABAa = h.GABAa_S(0.5, sec=self.soma)
		
		# needed for PyNN
		self.source_section = self.soma
		self.source = self.soma(0.5)._ref_v
		self.rec = h.NetCon(self.source, None, sec=self.soma)		# Needed to clear the simulator
		self.spike_times = h.Vector(0)
		self.parameter_names = ('L', 'diam', 'nseg', 'Ra', 'cm', 'bias_current_density')
		self.traces = {}
		self.recording_time = False

	L = _new_property('soma', 'L')
	diam = _new_property('soma', 'diam')
	nseg = _new_property('soma', 'nseg')
	Ra = _new_property('soma', 'Ra')
	cm = _new_property('soma', 'cm')
	#bias_current_density = _new_property('stim', 'amp') - commented out thalamic bias current for efficiency
	
	def area(self):
		"""Membrane area in µm²"""
		return pi * self.soma.L * self.soma.diam
	
	def memb_init(self):
		for seg in self.soma:
			seg.v = self.v_init
	

class Thalamic_Neuron_Type(NativeCellType):
	default_parameters = {'L': 100, 'diam': 100, 'nseg': 1, 'Ra': 150, 'cm': 100, 'bias_current_density': 0}
	default_initial_values = {'v': -68.0}
	recordable = ['soma(0.5).v']
	units = {'soma(0.5).v' : 'mV'}    
	receptor_types = ['AMPA', 'GABAa']
	model = Thalamic_Neuron