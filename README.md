Cortical Network + FE Coupled Model - Axon Model

Cortical Network model with axon coupled to FE model for testing variation in FE Model.

Model Requirements: - Model is simulated using PyNN with NEURON as it's backend simulator, and so follow their instatllation instructions at: 1) Neuron - https://www.neuron.yale.edu/neuron/download 2) PyNN - https://pypi.org/project/PyNN/ - http://neuralensemble.org/docs/PyNN/

Model Setup: 1) Copy included PyNN files in the repo to their corresponding location from the PyNN instatllation - Needed for correct simulation of the multicompartmental cortical neurons 2) Compile the NEURON model mod files using either mknrndll or nrnivmodl, for windows or Linux, respectively. 3) Run run_Cortical_Axon_Model.py Example: 1) From the command line/terminal navigate to the folder containing the model. 2) Execute "python run_Cortical_Axon_Model.py neuron"
model from this point.
Running the Model: - navigate to the model directory in the command line and type the following command to run the model:

"python run_Cortical_Axon_Model.py neuron"

Model Outputs, i.e. the cortical soma, collateral and interneuron membrane voltages are saved as .mat files. Analysis of the membrane voltages is then done using Matlab.

Note - must update the directory of where output data from the simulation will be written to (located at the end of the file).

