%% Load Cortical data and plot
%% Cortical Neuron Somas:
load('Cortical_Soma_v.mat')
neo_block = block.segments{1}.analogsignals{1};
v_Cortical_Soma = neo_block.signal;

fs = 2000;
t = [0:length(v_Cortical_Soma(:,1))-1]*(1/fs);

figure()
plot(t,v_Cortical_Soma(:,3))
xlabel('Time (sec))')
ylabel('Voltages (mV)')
title('Single Cortical Soma Membrane Potential')

Cortical_Soma_Spike_Train = Calculate_pop_spike_train(v_Cortical_Soma',0,0.003*fs);
[rows,cols] = size(Cortical_Soma_Spike_Train);
figure()

hold on
for i=1:rows
    for j=1:cols
       if (Cortical_Soma_Spike_Train(i,j)==1)
            h1 = plot(t(j),i,'.k', 'MarkerSize', 6);
       end
    end
end
set(gca, 'FontName', 'Times New Roman', 'FontSize', 18, 'FontWeight', 'bold')

ylim([0 100])
yticks([0 50 100])

%% Cortical Neuron Collaterals:
load('Cortical_Collateral_v.mat')
neo_block = block.segments{1}.analogsignals{1};
v_Cortical_Collateral = neo_block.signal;

fs = 2000;
t = [0:length(v_Cortical_Collateral(:,1))-1]*(1/fs);

figure()
plot(t,v_Cortical_Collateral(:,3))
xlabel('Time (sec))')
ylabel('Voltages (mV)')
title('Single Cortical Collateral Membrane Potential')

Ctx_Collateral_Spike_Train = Calculate_pop_spike_train(v_Cortical_Collateral',0);
[rows,cols] = size(Ctx_Collateral_Spike_Train);
figure()
hold on
for i=1:rows
    for j=1:cols
       if (Ctx_Collateral_Spike_Train(i,j)==1)
          plot(t(j),i,'.k');
       end
    end
end

set(gca, 'FontName', 'Times New Roman', 'FontSize', 12, 'FontWeight', 'bold')
xlabel('Time (s)', 'FontSize', 14, 'FontWeight', 'bold')
ylabel('Neuron No.', 'FontSize', 14, 'FontWeight', 'bold')
title('Cortex Collateral', 'FontSize', 14, 'FontWeight', 'bold')
ax = gca;
set(gcf, 'Position', [100 300 1800 300])

window_length = fs;
overlap = 0.5;

% Plot Cortical Collateral PSD
figure();
[pxx_Cortical_Collateral,f_Cortical_Collateral] = pwelch(v_Cortical_Collateral,hamming(window_length),(window_length)*overlap,window_length,fs);
plot(f_Cortical_Collateral,mean(pxx_Cortical_Collateral, 2));
xlabel('Frequency (Hz)')
ylabel('PSD')
title('Cumulative Cortical Collateral PSD')
xlim([0 150])
ylim([0 5])